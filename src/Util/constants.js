export const BATSMEN = 'Batsman';
export const BOWLER = 'Bowler';
export const ALL_ROUNDER = 'All-Rounder';

export const MAX_BATSMEN = 3;
export const MAX_BOWLER = 2;
export const MAX_ALL_ROUNDER = 2;
