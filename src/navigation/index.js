import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

import PlayerState from '../context/PlayerState';
import Players from '../screens/Players';
import SelectPlayer from '../screens/SelectPlayer';

const BottomTab = createBottomTabNavigator();

function Player() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Player">
        {(props) => (
          <PlayerState>
            <Players {...props} />
          </PlayerState>
        )}
      </Stack.Screen>
    </Stack.Navigator>
  );
}

function Selection() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Selection">
        {(props) => (
          <PlayerState>
            <SelectPlayer {...props} />
          </PlayerState>
        )}
      </Stack.Screen>
    </Stack.Navigator>
  );
}

const Home = () => {
  return (
    <BottomTab.Navigator>
      <BottomTab.Screen name={'Players'} component={Player} />
      <BottomTab.Screen name={'Select'} component={Selection} />
    </BottomTab.Navigator>
  );
};

export default Home;
