import React, {useContext, useEffect} from 'react';

import playerContext from '../context/PlayerContext';

import {FlatList, StyleSheet} from 'react-native';

import Header from '../component/players/Header';
import Item from '../component/players/Item';

const Players = () => {
  const PlayerContext = useContext(playerContext);
  useEffect(() => {
    PlayerContext.getPlayers();
  }, []);

  return (
    <FlatList
      style={styles.list}
      data={PlayerContext.players ? PlayerContext.players : []}
      ListHeaderComponent={<Header />}
      renderItem={(item) => <Item item={item} />}
    />
  );
};

const styles = StyleSheet.create({
  list: {
    flex: 1,
  },
});

export default Players;
