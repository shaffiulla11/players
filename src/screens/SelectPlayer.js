import React, {useState} from 'react';
import {View, Alert, TouchableOpacity, StyleSheet, Text} from 'react-native';
import _ from 'lodash';

import * as Constant from '../Util/constants';

const SelectPlayer = (props) => {
  const [players, setPlayers] = useState([]);
  const [batsmen, setBatsmen] = useState(0);
  const [bowler, setBowler] = useState(0);
  const [allRound, setAllRound] = useState(0);

  const validate = () => {
    if (players.length > 7) {
      Alert('Maximum of 7 players are allowed');
    }

    const points = _.sumBy(players, (item) => item.points);

    if (points > 75) {
      Alert(
        "You are exceeding points rules, please try with different combination and points shouldn't exceed 75",
      );
    }
    for (const player of players) {
      switch (player.role) {
        case Constant.BATSMEN:
          setBatsmen((previous) => previous + 1);
          break;
        case Constant.BOWLER:
          setBowler((previous) => previous + 1);
          break;
        case Constant.ALL_ROUNDER:
          setAllRound((previous) => previous + 1);
          break;
        default:
      }
    }

    if (batsmen > 3 || bowler > 2 || allRound > 2) {
      Alert('You cannot exceed allotted slots of players');
    } else {
      Alert('Successfully created your team 7');
    }
  };

  const onAdd = () => props.navigation.navigate('Players');
  return (
    <View style={styles.root}>
      <TouchableOpacity style={styles.add} onPress={onAdd}>
        <Text>{'+'}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  add: {
    width: '80%',
    height: 50,
    marginHorizontal: 30,
    backgroundColor: '#999999',
    alignItems: 'center',
    justifyContent: 'center'
  },
  root: {
    flex: 1,
  },
});
export default SelectPlayer;
