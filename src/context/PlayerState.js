import React, {useReducer, useState} from 'react';
import data from '../data/players.json';

import PlayerContext from './PlayerContext';
import PlayerReducer from './PlayerReducer';
import {GET_PLAYERS, GET_TEAM, ADD_PLAYER, REMOVE_PLAYER} from './types';

const PlayerState = (props) => {
  let initialState = {players: [], teams: []};

  const [state, dispatch] = useReducer(PlayerReducer, initialState);

  const getPlayers = () =>
    dispatch({
      type: GET_PLAYERS,
      payload: data,
    });

  const getTeam = () =>
    dispatch({
      type: GET_TEAM,
      payload: state.players.filter((item) => item.selected),
    });

  const selectPlayer = (player) => {
    const isExist = state.team.indexOf(player) !== -1;
    if (isExist) {
      dispatch({type: REMOVE_PLAYER, payload: player});
    } else {
      dispatch({type: ADD_PLAYER, payload: player});
    }
  };
  return (
    <PlayerContext.Provider
      value={{
        players: state.players,
        team: state.team,
        getPlayers,
        getTeam,
        selectPlayer,
      }}>
      {props.children}
    </PlayerContext.Provider>
  );
};

export default PlayerState;
