import {GET_PLAYERS, GET_TEAM, ADD_PLAYER, REMOVE_PLAYER} from './types';

export default (state, action) => {
  const {payload, type} = action;

  switch (type) {
    case GET_PLAYERS:
      return {
        ...state,
        players: payload,
      };
    case GET_TEAM:
      return {
        ...state,
        team: payload,
      };
    case ADD_PLAYER:
      return {
        ...state,
        team: [...state.team, payload],
      };
    case REMOVE_PLAYER:
      const index = state.team.indexOf(payload);
      const team = state.team.splice(index, 1);
      console.log("index", index, state.team, team)
      return {
        ...state,
        team: [...team],
      };
    default:
      return state;
  }
};
