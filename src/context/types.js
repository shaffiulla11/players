export const GET_PLAYERS = 'GET_PLAYERS';
export const GET_TEAM = 'GET_TEAM';
export const ADD_PLAYER = 'ADD_PLAYER';
export const REMOVE_PLAYER = 'REMOVE_PLAYER';
