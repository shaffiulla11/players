import React, {useState, useContext, useEffect} from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import playerContext from '../../context/PlayerContext';

const Item = (props) => {
  const PlayerContext = useContext(playerContext);
  const [showDelete, setShowDelete] = useState(false);
  const {item} = props.item;

  useEffect(() => {
    PlayerContext.getTeam();
  }, []);

  const isSelected =
    PlayerContext.team.length > 0
      ? PlayerContext.team.indexOf(item) !== -1
      : false;

  const onClick = () => PlayerContext.selectPlayer(item);
  const onLongTap = () => setShowDelete(true);
  const onRemove = () => (item.selected = false);
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={[styles.row, isSelected && styles.selected]}
      onPress={onClick}
      onLongPress={onLongTap}>
      <Text style={styles.text}>{item.name}</Text>
      <Text style={styles.text}>{item.role}</Text>
      <Text style={styles.text}>{item.points}</Text>
      {showDelete && (
        <TouchableOpacity style={styles.delete} onPress={onRemove}>
          <Text>{'X'}</Text>
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 50,
    alignItems: 'center',
    marginHorizontal: 20,
  },
  selected: {
    backgroundColor: 'grey',
  },
  delete: {
    position: 'absolute',
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  text: {
    width: '33%',
    textAlign: 'left',
  },
});

export default Item;
