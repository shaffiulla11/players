import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Header = () => {
  return (
    <View style={styles.row}>
      <Text style={styles.text}>Name</Text>
      <Text style={styles.text}>Role</Text>
      <Text style={styles.text}>Points</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 50,
    alignItems: 'center',
    backgroundColor: '#aaaaaa',
    paddingHorizontal: 20
  },
  text:{
    width: '33%',
    textAlign: 'left'
  },
});
export default Header;
